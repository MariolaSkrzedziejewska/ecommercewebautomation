### What is this repository for? ###

Page Object Model framework for automated functional tests of page http://automationpractice.com - the sample real-time ecommerce website for automation testing practice.


### Technologies to use ###

• Java 11+

• Maven

• Selenium WebDriver v 3.141.59

• jUnit v 5.7.0

• Page object pattern

• Page factory

• java Faker v 1.0.2

### Project details ###
•Test scenarios classes inherit from BaseTest class

• BaseTest class contains method to easily switch between browsers (webdrivers):

-Chrome

-Chrome headless

-Firefox
	
• Test classes contains only test logic

• Every tested page has got corresponding page object class

• BasePage class with common page objects

• RandomUser class – object of that class are used for register tests

### Tests scope ###
• Login (positive, negative)

• Logout

• Register (positive, validations)

• Search items

• Navigation menu (visibility ofter mouse hover on menu)

• Add random items to cart and check cart value

• Remove random items from cart

• Make an order

• Add new address in my account

• Edit exist address in my account

• Delete exist address in my account


### How do I get set up? ###
a) download and install:

•Intellij IDE

•Java Development Kit (JDK)

•browsers

-Chrome

-Firefox

•drivers

-chrome driver 

-GeckoDriver
	
b) clone my project from this repository

c) to switch between browser (webdriver):

• find BaseTest class

• then Driver Setup method

• then in the line "driver = prepareDriver("chrome") you need to enter the name of the browser:

-chrome

-headless

or

-firefox
	

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact

[]: http://automationpractice.com