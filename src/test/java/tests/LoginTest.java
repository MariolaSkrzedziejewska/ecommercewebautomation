package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class LoginTest extends BaseTest {
    HomePage homePage = new HomePage(driver, wait);

    @Test
    void shouldLoginUserWhenCorrectCredentialsAreProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        Assertions.assertTrue(loginPage.isSignOutButtonDisplayed());
    }

    @Test
    void shouldDisplayEmailRequiredAlertWhenNoCredentialsAreProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("", "");
        Assertions.assertTrue(loginPage.isEmailRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayAuthenticationAlertWhenWrongPasswordIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "wrong_password");
        Assertions.assertTrue(loginPage.isAuthenticationErrorAlertDisplayed());
    }

    @Test
    void shouldDisplayPasswordIsRequiredWhenNoPasswordIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "");
        Assertions.assertTrue(loginPage.isPasswordIsRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayEmailIsRequiredWhenNoEmailIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("", "password");
        Assertions.assertTrue(loginPage.isEmailIsRequiredAlertDisplayed());
    }

    @Test
    void shouldDisplayInvalidEmailAlertWhenWrongEmailIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("wrongemail", "password");
        Assertions.assertTrue(loginPage.isInvalidEmailAlertDisplayed());
    }
}