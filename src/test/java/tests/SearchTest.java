package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.SearchPage;

public class SearchTest extends BaseTest {
    BasePage basePage = new BasePage(driver, wait);

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForExistingProduct() {
        BasePage.open();
        SearchPage searchPage = basePage.searchForProduct("dress");
        Assertions.assertTrue(searchPage.getSearchResultProductNumber() > 0);
    }

    @Test
    void shouldReturnEmptySearchResultsWhenLookingForNonExistingProduct() {
        BasePage.open();
        SearchPage searchPage = basePage.searchForProduct("drill");
        Assertions.assertEquals(searchPage.getSearchResultProductNumber(), 0);
    }

    @Test
    void shouldReturnNonEmptySearchResultsWhenLookingForExistingProduct2() {
        BasePage.open();
        SearchPage searchPage = basePage.searchForProduct("blouse");
        Assertions.assertEquals("1 result has been found.", searchPage.getSearchResultText());
        Assertions.assertEquals(searchPage.getSearchResultProductNumber(), 1);
    }
}
