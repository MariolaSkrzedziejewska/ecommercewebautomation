package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;

import static pageobjects.BasePage.*;

public class NavigationTest extends BaseTest {
    HomePage homePage = new HomePage(driver, wait);

    @Test
    void shouldDisplayDressesSubmenuAfterMouseHoverOnMenu() {
        open();
        Assertions.assertTrue(homePage.mouseHoverOnDressesMenu().isDisplayed());
    }
}
