package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.ShoppingPage;

public class ShoppingTest extends BaseTest {
    ShoppingPage shoppingPage = new ShoppingPage(driver, wait);
    HomePage homePage = new HomePage(driver, wait);

    @Test
    void shouldDisplayCorrectTotalCostInBasket() {
        ShoppingPage.open();
        shoppingPage.addRandomProductsToCart(3);
        Assertions.assertEquals(shoppingPage.getExpectedCartValue(), shoppingPage.getCurrentCartValue());
    }

    @Test
    void shouldMakeAnOrderForAddedProducts() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");

        ShoppingPage.open();
        shoppingPage.addRandomProductsToCart(3);
        shoppingPage.makeAnOrderForAddedProducts();
        Assertions.assertTrue(shoppingPage.getOrderConfirmationText().contains("Your order on My Store is complete."));
    }

    @Test
    void shouldDisplayTermsOfServiceAlertWhenAgreementCheckboxUnchecked() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");

        ShoppingPage.open();
        shoppingPage.addRandomProductsToCart(3);
        shoppingPage.makeAnOrderWhenAgreementCheckboxUnchecked();
        Assertions.assertTrue(shoppingPage.getTermsOfServiceAlertText().contains("You must agree to the terms of service before continuing."));
    }

    @Test
    void shouldDisplayEmptyCartWhenRemoveUniqueItemFromCart() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");

        ShoppingPage.open();
        shoppingPage.addRandomProductsToCart(1);
        shoppingPage.removeRandomItem();
        Assertions.assertTrue(shoppingPage.getShoppingAlertText().contains("Your shopping cart is empty."));
    }

    @Test
    void shouldRemoveRandomItemWhenCartIsNotEmpty() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");

        ShoppingPage.open();
        shoppingPage.addRandomProductsToCart(5);
        shoppingPage.removeRandomItem();
        Assertions.assertTrue(shoppingPage.getNumberOfTrashButtons() <= shoppingPage.getShoppingCartContainsResultText());
    }
}