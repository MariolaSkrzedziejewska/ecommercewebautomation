package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.*;
import utils.RandomUser;

public class MyAccountTest extends BaseTest {
    HomePage homePage = new HomePage(driver, wait);
    MyAccountPage myAccountPage = new MyAccountPage(driver, wait);
    RandomUser randomUser = new RandomUser();

    @Test
    void shouldAddNewAddressWhenCorrectDataIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        myAccountPage.goToMyAddresses();
        myAccountPage.addNewAddress(randomUser);
        Assertions.assertTrue(myAccountPage.addNewAddressButtonIsDisplayed());
    }

    @Test
    void shouldDisplayAliasErrorWhenAliasAddressIsRepeated() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        randomUser.addressTitle = "";
        myAccountPage.goToMyAddresses();
        myAccountPage.addNewAddress(randomUser);
        Assertions.assertTrue(myAccountPage.getRegisterErrorText().
                contains("The alias \"My address\" has already been used. Please select another one."));
    }

    @Test
    void shouldEditFirstDisplayedAddressWhenCorrectDataIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        myAccountPage.goToMyAddresses();
        myAccountPage.editFirstDisplayedAddress(randomUser);
        Assertions.assertTrue(myAccountPage.addNewAddressButtonIsDisplayed());
    }

    @Test
    void shouldEditRandomAddressWhenCorrectDataIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        myAccountPage.goToMyAddresses();
        myAccountPage.editRandomAddress(randomUser);
        Assertions.assertTrue(myAccountPage.addNewAddressButtonIsDisplayed());
    }

    @Test
    void shouldDeleteRandomAddressWhenDeleteButtonIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        myAccountPage.goToMyAddresses();
        int numberOfDeleteButtons = myAccountPage.getNumberOfDeleteButtons();
        myAccountPage.deleteRandomAddress();
        Assertions.assertTrue((numberOfDeleteButtons - 1) <= myAccountPage.getNumberOfDeleteButtons());
    }
}