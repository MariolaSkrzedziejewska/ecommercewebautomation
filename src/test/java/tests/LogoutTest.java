package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class LogoutTest extends BaseTest {
    HomePage homePage = new HomePage(driver, wait);

    @Test
    void shouldLogOutUserWhenSignOutButtonIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        loginPage.login("martest@testowo.pl", "test123");
        loginPage.clickSignOnButton();
        Assertions.assertTrue(loginPage.isSignInButtonDisplayed());
    }
}
