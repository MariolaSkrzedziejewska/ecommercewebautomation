package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;
import utils.RandomUser;

public class RegisterTest extends BaseTest {
    HomePage homePage = new HomePage(driver, wait);
    RandomUser randomUser = new RandomUser();

    @Test
    void shouldRegisterNewUserWhenCorrectDataIsUsed() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        Assertions.assertTrue(registerPage.isSignOutButtonDisplayed());
    }

    @Test
    void shouldDisplayZipCodeErrorWhenTooShortZipCodeIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        randomUser.postcode = "11";
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        Assertions.assertEquals("The Zip/Postal code you've entered is invalid. It must follow this format: 00000", registerPage.getRegisterErrorText());
    }

    @Test
    void shouldDisplayMobilePhoneErrorWhenOnlyTextIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        randomUser.phone = "number";
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        Assertions.assertEquals("phone_mobile is invalid.", registerPage.getRegisterErrorText());
    }

    @Test
    void shouldDisplayPasswordErrorWhenTooShortPasswordIsProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        randomUser.password = "weak";
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        Assertions.assertEquals("passwd is invalid.", registerPage.getRegisterErrorText());
    }

    @Test
    void shouldDisplayRegisterErrorTextWhenIncompleteFieldsAreProvided() {
        BasePage.open();
        LoginPage loginPage = homePage.goToLogin();
        randomUser.firstName = "";
        randomUser.address1 = "";
        randomUser.phone = "";
        RegisterPage registerPage = loginPage.goToRegister(randomUser.email);
        registerPage.register(randomUser);
        Assertions.assertTrue(registerPage.getRegisterErrorText().contains("You must register at least one phone number."));
        Assertions.assertTrue(registerPage.getRegisterErrorText().contains("firstname is required."));
        Assertions.assertTrue(registerPage.getRegisterErrorText().contains("address1 is required."));
    }
}