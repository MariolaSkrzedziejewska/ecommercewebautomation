package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    protected static WebDriver driver;
    protected WebDriverWait wait;
    private static final String BASE_URL = "http://automationpractice.com/index.php";

    @FindBy(linkText = "Sign in")
    private WebElement signInButton;

    @FindBy(className = "logout")
    private WebElement signOutButton;

    @FindBy(id = "search_query_top")
    private WebElement searchInputField;

    @FindBy(id = "address1")
    protected WebElement address1;

    @FindBy(id = "city")
    protected WebElement city;

    @FindBy(id = "id_state")
    protected WebElement id_state;

    @FindBy(id = "postcode")
    protected WebElement postcode;

    @FindBy(id = "phone_mobile")
    protected WebElement phone_mobile;

    @FindBy(xpath = "//div[@class='alert alert-danger']//ol")
    protected WebElement registerErrorText;

    public BasePage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        PageFactory.initElements(driver, this);
    }

    public static void open() {
        driver.get(BASE_URL);
    }

    public LoginPage goToLogin() {
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));
        signInButton.click();
        return new LoginPage(driver, wait);
    }

    public boolean isSignOutButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(signOutButton));
        return signOutButton.isDisplayed();
    }

    public boolean isSignInButtonDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(signInButton));
        return signInButton.isDisplayed();
    }

    public void clickSignOnButton() {
        wait.until(ExpectedConditions.visibilityOf(signOutButton));
        signOutButton.click();
    }

    public SearchPage searchForProduct(String productName) {
        wait.until(ExpectedConditions.elementToBeClickable(searchInputField));
        searchInputField.clear();
        searchInputField.sendKeys(productName);
        searchInputField.sendKeys(Keys.ENTER);
        return new SearchPage(driver, wait);
    }

    public String getRegisterErrorText() {
        wait.until(ExpectedConditions.visibilityOf(registerErrorText));
        return registerErrorText.getText();
    }
}
