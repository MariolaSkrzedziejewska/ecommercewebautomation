package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(linkText = "DRESSES")
    private WebElement dressesMenu;

    @FindBy(css = "li ul.submenu-container")
    private List<WebElement> submenu;

    public WebElement mouseHoverOnDressesMenu() {
        Actions builder = new Actions(driver);
        builder.moveToElement(dressesMenu).perform();
        wait.until(ExpectedConditions.visibilityOf(submenu.get(1)));
        return submenu.get(1);
    }
}