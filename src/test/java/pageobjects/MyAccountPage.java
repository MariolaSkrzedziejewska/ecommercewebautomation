package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

import java.util.List;
import java.util.Random;

public class MyAccountPage extends BasePage {

    public MyAccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    Random rnd = new Random();

    @FindBy(xpath = "//*[@title='Addresses']")
    private WebElement myAddressesButton;

    @FindBy(xpath = "//*[@title='Add an address']")
    private WebElement addNewAddressButton;

    @FindBy(id = "alias")
    private WebElement addressTitleAlias;

    @FindBy(linkText = "Update")
    private List<WebElement> updateButton;

    @FindBy(linkText = "Delete")
    private List<WebElement> deleteButton;

    public void addNewAddress(RandomUser randomUser) {
        addNewAddressButton.click();
        address1.sendKeys(randomUser.address1);
        city.sendKeys(randomUser.city);
        Select stateSelect = new Select(id_state);
        stateSelect.selectByVisibleText(randomUser.state);
        postcode.sendKeys(randomUser.postcode);
        phone_mobile.sendKeys(randomUser.phone);
        addressTitleAlias.sendKeys(randomUser.addressTitle);
        phone_mobile.sendKeys(Keys.ENTER);
    }

    public boolean addNewAddressButtonIsDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(addNewAddressButton));
        return addNewAddressButton.isDisplayed();
    }

    public void editFirstDisplayedAddress(RandomUser randomUser) {
        updateButton.get(0).click();
        city.clear();
        city.sendKeys(randomUser.city);
        city.sendKeys(Keys.ENTER);
    }

    public void editRandomAddress(RandomUser randomUser) {
        int randomUpdateButtonIndex = rnd.nextInt(updateButton.size());
        wait.until(ExpectedConditions.elementToBeClickable((updateButton.get(randomUpdateButtonIndex))));
        updateButton.get(randomUpdateButtonIndex).click();
        address1.clear();
        address1.sendKeys(randomUser.address1);
        address1.sendKeys(Keys.ENTER);
    }

    public void deleteRandomAddress() {
        int randomDeleteButtonIndex = rnd.nextInt(deleteButton.size());
        wait.until(ExpectedConditions.elementToBeClickable(deleteButton.get(randomDeleteButtonIndex)));
        deleteButton.get(randomDeleteButtonIndex).click();
        driver.switchTo().alert().accept();
    }

    public int getNumberOfDeleteButtons() {
        return deleteButton.size();
    }

    public void goToMyAddresses() {
        wait.until(ExpectedConditions.elementToBeClickable(myAddressesButton));
        myAddressesButton.click();
    }
}