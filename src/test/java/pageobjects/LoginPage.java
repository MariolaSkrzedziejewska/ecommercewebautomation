package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(id = "email")
    private WebElement loginField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "SubmitLogin")
    private WebElement submitLoginButton;

    @FindBy(className = "alert-danger")
    private WebElement authenticationAlert;

    @FindBy(id = "email_create")
    private WebElement emailCreateField;

    @FindBy(id = "SubmitCreate")
    private WebElement submitCreate;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void login(String email, String password) {
        loginField.sendKeys(email);
        passwordField.sendKeys(password);
        submitLoginButton.click();
    }

    public boolean isEmailRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("An email address required.");
    }

    public boolean isAuthenticationErrorAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Authentication failed.");
    }

    public boolean isPasswordIsRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Password is required.");
    }

    public boolean isEmailIsRequiredAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("An email address required.");
    }

    public boolean isInvalidEmailAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authenticationAlert));
        return authenticationAlert.getText().contains("Invalid email address.");
    }

    public RegisterPage goToRegister(String email) {
        wait.until(ExpectedConditions.elementToBeClickable(emailCreateField));
        emailCreateField.sendKeys(email);
        submitCreate.click();
        return new RegisterPage(driver, wait);
    }
}