package pageobjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;

public class ShoppingPage extends BasePage {

    private static final String CATEGORY_URL = "http://automationpractice.com/index.php?id_category=3&controller=category";

    public ShoppingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    Random rnd = new Random();

    public static void open() {
        driver.get(CATEGORY_URL);
    }

    private double expectedCartValue = 2;

    @FindBy(xpath = "//div[@class='product-container']")
    private List<WebElement> products;

    @FindBy(xpath = "//a[contains(@class,'ajax_add_to_cart_button')]")
    private List<WebElement> addToCartButtons;

    @FindBy(xpath = "//div[@class='right-block']//span[@class='price product-price']")
    private List<WebElement> productPrices;

    @FindBy(xpath = "//div[@class='shopping_cart']//b")
    private WebElement cartButton;

    @FindBy(xpath = "//span[@title='Continue shopping']")
    private WebElement continueShoppingButton;

    @FindBy(xpath = "//span[contains(@class,'cart_block_total')]")
    private WebElement cartTotalPrice;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']//a[@title='Proceed to checkout']")
    private WebElement proceedToCheckoutButton1;

    @FindBy(xpath = "//p[@class='cart_navigation clearfix']/button[@type='submit']")
    private WebElement proceedToCheckoutButton2;

    @FindBy(id = "cgv")
    private WebElement termsCheckbox;

    @FindBy(xpath = "//button[@name='processCarrier']")
    private WebElement proceedToCheckoutButton3;

    @FindBy(xpath = "//a[@class='bankwire']")
    private WebElement payByBankWireButton;

    @FindBy(xpath = "//div[@class='box']")
    private WebElement orderConfirmationText;

    @FindBy(xpath = "//div[@class='fancybox-inner']")
    private WebElement termsOfServiceAlertText;

    @FindBy(xpath = "//a[contains(@class,'cart_quantity_delete')]")
    private List<WebElement> trashButton;

    @FindBy(xpath = "//*[@class='alert alert-warning']")
    private WebElement shoppingAlertText;

    @FindBy(css = ".shopping_cart .ajax_cart_quantity")
    private WebElement shoppingCartContainsResultText;

    public void addRandomProductsToCart(int numberOfItems) {
        for (int i = 0; i < numberOfItems; i++) {
            int randomProductIndex = rnd.nextInt(products.size());
            wait.until(ExpectedConditions.visibilityOf(productPrices.get(randomProductIndex)));
            String actualProductPrice = productPrices.get(randomProductIndex).getText().replace("$", "");
            expectedCartValue += Double.parseDouble(actualProductPrice);
            moveMouseToProduct(randomProductIndex);
            wait.until(ExpectedConditions.visibilityOf(addToCartButtons.get(randomProductIndex)));
            addToCartButtons.get(randomProductIndex).click();
            wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton));
            continueShoppingButton.click();
        }
    }

    private void moveMouseToProduct(int randomProductIndex) {
        wait.until(ExpectedConditions.visibilityOf(products.get(randomProductIndex)));
        Actions builder = new Actions(driver);
        scrollIntoView(products.get(randomProductIndex));
        builder.moveToElement(products.get(randomProductIndex)).build().perform();
    }

    public double getExpectedCartValue() {
        return round(expectedCartValue, 2);
    }

    public double getCurrentCartValue() {
        Actions builder = new Actions(driver);
        scrollIntoView(cartButton);
        builder.moveToElement(cartButton).build().perform();
        wait.until(ExpectedConditions.visibilityOf(cartTotalPrice));
        return Double.parseDouble(cartTotalPrice.getText().replace("$", ""));
    }

    private static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void makeAnOrderForAddedProducts() {
        goToCart();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton1));
        proceedToCheckoutButton1.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton2));
        proceedToCheckoutButton2.click();
        termsCheckbox.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton3));
        proceedToCheckoutButton3.click();
        wait.until(ExpectedConditions.elementToBeClickable(payByBankWireButton));
        payByBankWireButton.click();
        proceedToCheckoutButton2.click();
    }

    public String getOrderConfirmationText() {
        wait.until(ExpectedConditions.visibilityOf(orderConfirmationText));
        return orderConfirmationText.getText();
    }

    public void makeAnOrderWhenAgreementCheckboxUnchecked() {
        goToCart();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton1));
        proceedToCheckoutButton1.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton2));
        proceedToCheckoutButton2.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton3));
        proceedToCheckoutButton3.click();
    }

    public String getTermsOfServiceAlertText() {
        wait.until(ExpectedConditions.visibilityOf(termsOfServiceAlertText));
        return termsOfServiceAlertText.getText();
    }

    public void removeRandomItem() {
        goToCart();
        int randomDeleteItemButtonIndex = rnd.nextInt(trashButton.size());
        wait.until(ExpectedConditions.elementToBeClickable(trashButton.get(randomDeleteItemButtonIndex)));
        trashButton.get(randomDeleteItemButtonIndex).click();
    }

    public String getShoppingAlertText() {
        wait.until(ExpectedConditions.visibilityOf(shoppingAlertText));
        return shoppingAlertText.getText();
    }

    public int getNumberOfTrashButtons() {
        int randomDeleteItemButtonIndex = rnd.nextInt(trashButton.size());
        wait.until(ExpectedConditions.visibilityOf(trashButton.get(randomDeleteItemButtonIndex)));
        return trashButton.size();
    }

    public int getShoppingCartContainsResultText() {
        wait.until((ExpectedConditions.visibilityOf(shoppingCartContainsResultText)));
        return Integer.parseInt(shoppingCartContainsResultText.getText());
    }

    public void goToCart() {
        wait.until(ExpectedConditions.elementToBeClickable(cartButton));
        cartButton.click();
    }

    private void scrollIntoView(WebElement webElement) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
    }
}