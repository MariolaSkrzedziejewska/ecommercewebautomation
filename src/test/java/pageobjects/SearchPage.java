package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SearchPage extends BasePage {
    public SearchPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(xpath = "//span[@class='heading-counter']")
    private WebElement searchResultText;

    @FindBy(xpath = "//div[@class='product-container']")
    private List<WebElement> searchResults;

    public int getSearchResultProductNumber() {
        wait.until(ExpectedConditions.visibilityOf(searchResultText));
        return searchResults.size();
    }

    public String getSearchResultText() {
        wait.until(ExpectedConditions.visibilityOf(searchResultText));
        return searchResultText.getText();
    }
}
