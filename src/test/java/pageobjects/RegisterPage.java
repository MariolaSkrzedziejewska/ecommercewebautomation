package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.RandomUser;

public class RegisterPage extends BasePage {

    @FindBy(id = "customer_firstname")
    private WebElement customer_firstname;

    @FindBy(id = "customer_lastname")
    private WebElement customer_lastname;

    @FindBy(id = "passwd")
    private WebElement passwd;

    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public void register(RandomUser randomUser) {
        customer_firstname.sendKeys(randomUser.firstName);
        customer_lastname.sendKeys(randomUser.lastName);
        passwd.sendKeys(randomUser.password);
        address1.sendKeys(randomUser.address1);
        city.sendKeys(randomUser.city);
        Select stateSelect = new Select(id_state);
        stateSelect.selectByVisibleText(randomUser.state);
        postcode.sendKeys(randomUser.postcode);
        phone_mobile.sendKeys(randomUser.phone);
        phone_mobile.sendKeys(Keys.ENTER);
    }
}
