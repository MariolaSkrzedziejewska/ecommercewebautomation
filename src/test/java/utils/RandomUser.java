package utils;

import com.github.javafaker.Faker;

import java.sql.Timestamp;

public class RandomUser {
    public String firstName;
    public String lastName;
    public String email;
    public String password = "test123";
    public String address1;
    public String city;
    public String state;
    public String postcode;
    public String phone;
    public String addressTitle;

    public RandomUser() {
        Faker faker = new Faker();
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        email = lastName + timestamp.getTime() + "@wp.pl";
        address1 = faker.address().streetAddress();
        city = faker.address().city();
        state = faker.address().state();
        postcode = String.valueOf(faker.random().nextInt(11111, 99999));
        phone = String.valueOf(faker.phoneNumber().cellPhone());
        addressTitle = faker.lorem().fixedString(5);
    }
}
